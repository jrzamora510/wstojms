package cnoc.ws.objects;

public class CreationCnoc {

	private String cnocId = null;
    private String cnocDate = null;
    private String description = null;
    private String incidentArea = null;
    private String incidentStatus = null;
    private String cnocImpact = null;
    private String cnocUrgency = null;
    private String cnocPriority = null;
    private String cnocSeverity = null;
    private String cnocSupport3 = null;
    private String cnocProductType = null;
    private String reference = null;
    private String techContactName = null;
    private String techContactPhone = null;
    private String customer = null;
	
    
    public String getCnocId() {
		return cnocId;
	}
	public void setCnocId(String cnocId) {
		this.cnocId = cnocId;
	}
	public String getCnocDate() {
		return cnocDate;
	}
	public void setCnocDate(String cnocDate) {
		this.cnocDate = cnocDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIncidentArea() {
		return incidentArea;
	}
	public void setIncidentArea(String incidentArea) {
		this.incidentArea = incidentArea;
	}
	public String getIncidentStatus() {
		return incidentStatus;
	}
	public void setIncidentStatus(String incidentStatus) {
		this.incidentStatus = incidentStatus;
	}
	public String getCnocImpact() {
		return cnocImpact;
	}
	public void setCnocImpact(String cnocImpact) {
		this.cnocImpact = cnocImpact;
	}
	public String getCnocUrgency() {
		return cnocUrgency;
	}
	public void setCnocUrgency(String cnocUrgency) {
		this.cnocUrgency = cnocUrgency;
	}
	public String getCnocPriority() {
		return cnocPriority;
	}
	public void setCnocPriority(String cnocPriority) {
		this.cnocPriority = cnocPriority;
	}
	public String getCnocSeverity() {
		return cnocSeverity;
	}
	public void setCnocSeverity(String cnocSeverity) {
		this.cnocSeverity = cnocSeverity;
	}
	public String getCnocSupport3() {
		return cnocSupport3;
	}
	public void setCnocSupport3(String cnocSupport3) {
		this.cnocSupport3 = cnocSupport3;
	}
	public String getCnocProductType() {
		return cnocProductType;
	}
	public void setCnocProductType(String cnocProductType) {
		this.cnocProductType = cnocProductType;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getTechContactName() {
		return techContactName;
	}
	public void setTechContactName(String techContactName) {
		this.techContactName = techContactName;
	}
	public String getTechContactPhone() {
		return techContactPhone;
	}
	public void setTechContactPhone(String techContactPhone) {
		this.techContactPhone = techContactPhone;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
    
    
	
	
}
