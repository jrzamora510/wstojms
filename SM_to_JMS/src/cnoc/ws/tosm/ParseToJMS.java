package cnoc.ws.tosm;

import javax.jws.WebService;
import org.apache.log4j.Logger;
import cnoc.ws.jms.PublicadorGateway;
import cnoc.ws.objects.CreationCnoc;
import cnoc.ws.objects.UpdateCnoc;

@WebService(targetNamespace = "http://tosm.ws.cnoc/", portName = "ParseToJMSPort", serviceName = "ParseToJMSService")
public class ParseToJMS {
	
	//http://localhost:8080/SM_to_JMS/services/ParseToJMSPort?wsdl
	private static Logger log = Logger.getLogger(ParseToJMS.class);
	
	
	public String createJMS(CreationCnoc creationCnoc){
		
		System.out.println("*****************CREATE JMS *****************");
        System.out.println(creationCnoc.getCnocId());
        System.out.println(creationCnoc.getCnocDate());
        System.out.println(creationCnoc.getDescription());
        System.out.println(creationCnoc.getIncidentArea());
        System.out.println(creationCnoc.getIncidentStatus());
        System.out.println(creationCnoc.getCnocImpact());
        System.out.println(creationCnoc.getCnocUrgency());
        System.out.println(creationCnoc.getCnocPriority());
        System.out.println(creationCnoc.getCnocSeverity());
        System.out.println(creationCnoc.getCnocSupport3());
        System.out.println(creationCnoc.getCnocProductType());
        System.out.println(creationCnoc.getReference());
        System.out.println(creationCnoc.getTechContactName());
        System.out.println(creationCnoc.getTechContactPhone());
        System.out.println(creationCnoc.getCustomer());
        System.out.println("**********************************************");
		
		String msgRetCreate = null;
		boolean flgCreate = false;
		
		PublicadorGateway publicadorGateway = new PublicadorGateway();
		flgCreate = publicadorGateway.publicacionCreate(creationCnoc);
		
		if(flgCreate){
			log.info(creationCnoc.getCnocId() +  " Create message JMS sent to: " + creationCnoc.getCnocSupport3());
			msgRetCreate = "SUCCESS";
		}else{
			log.error(creationCnoc.getCnocId() +  " Create message JMS could not send to: " + creationCnoc.getCnocSupport3());
			msgRetCreate = "FAIL";
		}	
		
		
		return msgRetCreate;
	}
	
	
	public String updateJMS(UpdateCnoc updateCnoc){
		
		System.out.println("*****************UPDATE JMS *****************");
		System.out.println(updateCnoc.getCnocTckId());
        System.out.println(updateCnoc.getCitiTckId());
        System.out.println(updateCnoc.getCountryFsys());
        System.out.println(updateCnoc.getFsysTckId());
        System.out.println(updateCnoc.getPriority());
        System.out.println(updateCnoc.getTckStatus());
        System.out.println(updateCnoc.getTrackNotes());
        System.out.println(updateCnoc.getTypetrackNotes());
        System.out.println(updateCnoc.getResolution());
        System.out.println(updateCnoc.getTechContactNameCnoc());
        System.out.println(updateCnoc.getTechContactPhoneCnoc());
        System.out.println(updateCnoc.getClosureByClient());
        System.out.println(updateCnoc.getOrderUpdateDTime());
        System.out.println(updateCnoc.getSeverity());
        System.out.println("**********************************************");
		
		String msgRetUpdate = null;
		boolean flgUpdate = false;
		
		PublicadorGateway publicadorGateway = new PublicadorGateway();
		flgUpdate = publicadorGateway.publicacionUpdate(updateCnoc);
		
		if(flgUpdate){
			log.info(updateCnoc.getCnocTckId() +  " Message JMS sent to: " + updateCnoc.getTypetrackNotes());
			msgRetUpdate = "SUCCESS";
		}else{
			log.error(updateCnoc.getCnocTckId() +  " Message JMS could not send to: " + updateCnoc.getTypetrackNotes());
			msgRetUpdate = "FAIL";
		}	
		
		
		return msgRetUpdate;
	}

}
