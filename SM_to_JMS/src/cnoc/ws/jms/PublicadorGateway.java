package cnoc.ws.jms;


import javax.jms.*;

import cnoc.ws.objects.CreationCnoc;
import cnoc.ws.objects.UpdateCnoc;

import com.ibm.mq.jms.*;


public class PublicadorGateway {

	private String connectionFactoryName = "";
	private int mqPort = 0;
	private String hostName = "";
	private String mqChannel = "";
	private String usr = "";
	private String pass = "";
	private String nameQueue = "";
	
	private MQQueueConnectionFactory qcfS = null;
	private QueueConnection conS = null;
	private QueueSession sessionS = null;
	private MQQueue queueS = null;
	private QueueSender sender = null;
	
	private QueueConnection conE = null;
	private QueueSession sessionE = null;
	private QueueReceiver receiver = null;
	
	public boolean publicacionCreate(CreationCnoc forma){
		boolean resp = false;
		try{
			Properties.mainInit();
			String xml = armaXMLCreate(forma);
			connectionFactoryName = noNuloT(Properties.getProperty("connectionFactoryName"));
			mqPort= noNuloE(Properties.getProperty("mqPort"));
			hostName = noNuloT(Properties.getProperty("hostName"));
			mqChannel = noNuloT(Properties.getProperty("mqChannel"));
			usr = noNuloT(Properties.getProperty("usrMq"));
			pass = noNuloT(Properties.getProperty("passMq"));
			nameQueue = noNuloT(Properties.getProperty("nameQueue"));
			
			resp = sendText(xml, forma.getCnocId());
		}catch(Exception e){
			System.err.println(new StringBuffer("Publicador_MSERR.publicacionCreate()_Id: ").append(forma.getCnocId()).append("_").append(e).toString());
			
		}
		
		return resp;
	}
	
	public boolean publicacionUpdate(UpdateCnoc forma){
		boolean resp = false;
		try{
			Properties.mainInit();
			String xml = armaXMLUpdate(forma);
			connectionFactoryName = noNuloT(Properties.getProperty("connectionFactoryName"));
			mqPort= noNuloE(Properties.getProperty("mqPort"));
			hostName = noNuloT(Properties.getProperty("hostName"));
			mqChannel = noNuloT(Properties.getProperty("mqChannel"));
			usr = noNuloT(Properties.getProperty("usrMq"));
			pass = noNuloT(Properties.getProperty("passMq"));
			nameQueue = noNuloT(Properties.getProperty("nameQueue"));
			
			resp = sendText(xml, forma.getCnocTckId());
		}catch(Exception e){
			System.err.println(new StringBuffer("Publicador_MSERR.publicacionUpdate()_Id: ").append(forma.getCnocTckId()).append("_").append(e).toString());
			
		}
		
		return resp;
	}
	
	private String armaXMLCreate(CreationCnoc forma){
		StringBuffer xml = new StringBuffer(0);
		
		try{
			xml.append("<?xml version='1.0' encoding='UTF-8'?><CNOCCaseCreation>")
				.append("<CNOC_ID>").append(forma.getCnocId()).append("</CNOC_ID>")
				.append("<CNOC_Date>").append(forma.getCnocDate()).append("</CNOC_Date>")
				.append("<Description>").append(forma.getDescription()).append("</Description>")
				.append("<IncidentArea>").append(forma.getIncidentArea()).append("</IncidentArea>")
				.append("<IncidentStatus>").append(forma.getIncidentStatus()).append("</IncidentStatus>")
				.append("<CNOC_Impact>").append(forma.getCnocImpact()).append("</CNOC_Impact>")
				.append("<CNOC_Urgency>").append(forma.getCnocUrgency()).append("</CNOC_Urgency>")
				.append("<CNOC_Priority>").append(forma.getCnocPriority()).append("</CNOC_Priority>")
				.append("<CNOC_Severity>").append(forma.getCnocSeverity()).append("</CNOC_Severity>")
				.append("<CNOC_Support3>").append(forma.getCnocSupport3()).append("</CNOC_Support3>")
				.append("<CNOC_ProductType>").append(forma.getCnocProductType()).append("</CNOC_ProductType>")
				.append("<Reference>").append(forma.getReference()).append("</Reference>")
				.append("<TechnicalContactName>").append(forma.getTechContactName()).append("</TechnicalContactName>")
				.append("<TechnicalContactPhone>").append(forma.getTechContactPhone()).append("</TechnicalContactPhone>")
				.append("<Customer>").append(forma.getCustomer()).append("</Customer>")
			.append("</CNOCCaseCreation>");
		}catch(Exception e){
			System.err.println(new StringBuffer("PublicadorGateway_MSERR.armaXMLCreate()_Id: ").append(forma.getCnocId()).append("_").append(e).toString());
			
		}
	
		return xml.toString();
	}
	
	private String armaXMLUpdate(UpdateCnoc forma){
		StringBuffer xml = new StringBuffer(0);
		
		try{
			xml.append("<?xml version='1.0' encoding='UTF-8'?><TicketUpdateStatus>")
				.append("<CNOCTicketId>").append(forma.getCnocTckId()).append("</CNOCTicketId>")
				.append("<CitiTicketId>").append(forma.getCitiTckId()).append("</CitiTicketId>")
				.append("<CountryForeignSystem>").append(forma.getCountryFsys()).append("</CountryForeignSystem>")
				.append("<ForeignSystemTicketId>").append(forma.getFsysTckId()).append("</ForeignSystemTicketId>")
				.append("<Priority>").append(forma.getPriority()).append("</Priority>")
				.append("<TicketStatus>").append(forma.getTckStatus()).append("</TicketStatus>")
				.append("<TrackingNotes>").append(forma.getTrackNotes()).append("</TrackingNotes>")
				.append("<TypeofTrackingNotes>").append(forma.getTypetrackNotes()).append("</TypeofTrackingNotes>")
				.append("<Resolution>").append(forma.getResolution()).append("</Resolution>")
				.append("<TechnicalContactCNOC>").append(forma.getTechContactNameCnoc()).append("</TechnicalContactCNOC>")
				.append("<TechnicalContactPhoneCNOC>").append(forma.getTechContactPhoneCnoc()).append("</TechnicalContactPhoneCNOC>")
				.append("<ClosureByClient>").append(forma.getClosureByClient()).append("</ClosureByClient>")
				.append("<OrderUpdateDateTime>").append(forma.getOrderUpdateDTime()).append("</OrderUpdateDateTime>")
				.append("<Severity>").append(forma.getSeverity()).append("</Severity>")
			.append("</TicketUpdateStatus>");
		}catch(Exception e){
			System.err.println(new StringBuffer("PublicadorGateway_MSERR.armaXMLUpdate()_Id: ").append(forma.getCnocTckId()).append("_").append(e).toString());
			
		}
	
		return xml.toString();
	}
	
	private boolean sendText(String text, String id)throws Exception{
		boolean resp = false;
		try{
			qcfS = new MQQueueConnectionFactory();
			qcfS.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
			qcfS.setChannel(mqChannel);
			qcfS.setPort(mqPort);
			qcfS.setHostName(hostName);
			qcfS.setQueueManager(connectionFactoryName);
			conS = qcfS.createQueueConnection(usr,pass);
			conS.start();
			sessionS = conS.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			queueS = (MQQueue) sessionS.createQueue(nameQueue);
			
			queueS.setTargetClient(JMSC.MQJMS_CLIENT_NONJMS_MQ);
			
			sender = sessionS.createSender(queueS);
			
			TextMessage message = sessionS.createTextMessage();
			
			String expiracion = noNuloT(Properties.getProperty("expirationTime"));
			expiracion = expiracion.equals("") ? "0" : expiracion;
			long expira = Long.parseLong(expiracion);
			
			message.setText(text);
			message.setJMSExpiration(expira);
			message.setJMSPriority(1);
			
			sender.send(message);
			resp = true;
		}catch(Exception e){
			System.err.println(new StringBuffer("PublicadorGateway_MSERR.sendText()_Id: ").append(id).append("_").append(e).toString());
			
			throw e;
		}finally{
			try{if(sender != null){sender.close();}sender = null;}catch(Exception e){sender = null;}
			try{if(sessionS != null){sessionS.close();}sessionS = null;}catch(Exception e){sessionS = null;}
			try{if(receiver != null){receiver.close();}receiver = null;}catch(Exception e){receiver = null;}
			try{if(sessionE != null){sessionE.close();}sessionE = null;}catch(Exception e){sessionE = null;}
			try{if(conE != null){conE.close();}conE = null;}catch(Exception e){conE = null;}
			try{if(conS != null){conS.close();}conS = null;}catch(Exception e){conS = null;}
			queueS = null;
		}
		
		return resp;
	}
	
	private String noNuloT(Object cadena){
		return(cadena == null ? "" : cadena.toString().trim());
	}
	
	private int noNuloE(Object valorNumerico){
		int entero = 0;
		try{entero = Integer.parseInt(valorNumerico.toString());}catch(Exception e){entero = 0;}
		
		return entero;
	}
	
}
