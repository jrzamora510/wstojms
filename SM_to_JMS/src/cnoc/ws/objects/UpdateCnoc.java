package cnoc.ws.objects;

public class UpdateCnoc {
	
	private String cnocTckId = null;
    private String citiTckId = null;
    private String countryFsys = null;
    private String fsysTckId = null;
    private String priority = null;
    private String tckStatus = null;
    private String trackNotes = null;
    private String typetrackNotes = null;
    private String resolution = null;
    private String techContactNameCnoc = null;
    private String techContactPhoneCnoc = null;
    private String closureByClient = null;
    private String OrderUpdateDTime = null;
    private String severity = null;
    
    
	public String getCnocTckId() {
		return cnocTckId;
	}
	public void setCnocTckId(String cnocTckId) {
		this.cnocTckId = cnocTckId;
	}
	public String getCitiTckId() {
		return citiTckId;
	}
	public void setCitiTckId(String citiTckId) {
		this.citiTckId = citiTckId;
	}
	public String getCountryFsys() {
		return countryFsys;
	}
	public void setCountryFsys(String countryFsys) {
		this.countryFsys = countryFsys;
	}
	public String getFsysTckId() {
		return fsysTckId;
	}
	public void setFsysTckId(String fsysTckId) {
		this.fsysTckId = fsysTckId;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getTckStatus() {
		return tckStatus;
	}
	public void setTckStatus(String tckStatus) {
		this.tckStatus = tckStatus;
	}
	public String getTrackNotes() {
		return trackNotes;
	}
	public void setTrackNotes(String trackNotes) {
		this.trackNotes = trackNotes;
	}
	public String getTypetrackNotes() {
		return typetrackNotes;
	}
	public void setTypetrackNotes(String typetrackNotes) {
		this.typetrackNotes = typetrackNotes;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getTechContactNameCnoc() {
		return techContactNameCnoc;
	}
	public void setTechContactNameCnoc(String techContactNameCnoc) {
		this.techContactNameCnoc = techContactNameCnoc;
	}
	public String getTechContactPhoneCnoc() {
		return techContactPhoneCnoc;
	}
	public void setTechContactPhoneCnoc(String techContactPhoneCnoc) {
		this.techContactPhoneCnoc = techContactPhoneCnoc;
	}
	public String getClosureByClient() {
		return closureByClient;
	}
	public void setClosureByClient(String closureByClient) {
		this.closureByClient = closureByClient;
	}
	public String getOrderUpdateDTime() {
		return OrderUpdateDTime;
	}
	public void setOrderUpdateDTime(String orderUpdateDTime) {
		OrderUpdateDTime = orderUpdateDTime;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
    

}
