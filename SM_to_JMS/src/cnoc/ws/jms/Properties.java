package cnoc.ws.jms;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.PropertyResourceBundle;

public class Properties {

	private static PropertyResourceBundle properties = null;
	public Properties(){
		super();
	}
	
	public static void mainInit(){
		FileInputStream inputStream = null;
		try{
				
			inputStream = new FileInputStream("/opt/eBonding/conf_jms/configs.properties");
			properties = new PropertyResourceBundle(inputStream);
			
		}catch(IOException e){
			System.err.println(new StringBuffer("Properties_MSERR.mainInit(1)_").append(e).toString());
			
		}finally{
			try{
				inputStream.close();
			}catch(IOException e){
				System.err.println(new StringBuffer("Properties_MSERR.mainInit(2)_").append(e).toString());
				
			}
		}
	}
	
	public static String getProperty(String variable){
		String property = null;
		
		try{
			property = properties.getString(variable);
		}catch(Exception e){
			System.err.println(new StringBuffer("Properties_MSERR.getProperty(1)_").append(e).toString());
			
		}
		
		return property;
	}

	
}
